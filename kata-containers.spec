#needsrootforbuild
%global debug_package %{nil}
%global kernel_version 4.19.36

%define VERSION v1.11.1
%define RELEASE 6

Name:           kata-containers
Version:        %{VERSION}
Release:        %{RELEASE}
Summary:        Kata Container integration
License:        Apache 2.0
URL:            https://gitee.com/src-openeuler/kata_integration
Source0:        kata_integration-v1.0.0.tar.gz
Source1:        kata-runtime-%{version}.tar.gz
Source2:        kata-agent-%{version}.tar.gz
Source3:        kata-proxy-%{version}.tar.gz
Source4:        kata-shim-%{version}.tar.gz
Source5:        linux-%{kernel_version}.tar.gz

BuildRoot:      %_topdir/BUILDROOT
BuildRequires:  automake golang gcc bc glibc-devel glibc-static busybox glib2-devel glib2 ipvsadm conntrack-tools nfs-utils
BuildRequires:  patch elfutils-libelf-devel openssl-devel bison flex

%description
This is core component of Kata Container, to make it work, you need a docker engine.

%prep
%setup -T -c -a 0 -n %{name}-%{version}
%setup -T -c -a 1 -n runtime
%setup -T -c -a 2 -n agent
%setup -T -c -a 3 -n proxy
%setup -T -c -a 4 -n shim
%setup -T -c -a 5 -n kernel

# extract the kata_integration.tar.gz file
cd %{_builddir}/%{name}-%{version}
# apply kata_integration patches
sh apply-patches

# mv build components into kata_integration dir
pushd %{_builddir}/%{name}-%{version}
mv ../runtime .
mv ../agent .
mv ../proxy .
mv ../shim .
popd

# build kernel
cd %{_builddir}/kernel
mv kernel linux
cd %{_builddir}/kernel/linux/
%ifarch %{ix86} x86_64
cp %{_builddir}/%{name}-%{version}/hack/config-kata-x86_64 ./.config
%else
cp %{_builddir}/%{name}-%{version}/hack/config-kata-arm64 ./.config
%endif

%build
echo %{_builddir}
echo %{name}-%{version}
cd %{_builddir}/kernel/linux/
make %{?_smp_mflags}

cd %{_builddir}/%{name}-%{version}
mkdir -p -m 750 build
make runtime
make proxy
make shim
make initrd

%install
mkdir -p -m 755  %{buildroot}/var/lib/kata
%ifarch %{ix86} x86_64
install -p -m 755 -D %{_builddir}/kernel/linux/arch/x86_64/boot/bzImage %{buildroot}/var/lib/kata/kernel
%else
install -p -m 755 -D %{_builddir}/kernel/linux/arch/arm64/boot/Image %{buildroot}/var/lib/kata/kernel
%endif

cd %{_builddir}/%{name}-%{version}
mkdir -p -m 750  %{buildroot}/usr/bin
install -p -m 750 ./build/kata-runtime ./build/kata-proxy ./build/kata-shim ./build/kata-netmon %{buildroot}/usr/bin/
install -p -m 640 ./build/kata-containers-initrd.img %{buildroot}/var/lib/kata/
mkdir -p -m 750 %{buildroot}/usr/share/defaults/kata-containers/
install -p -m 640 -D ./runtime/cli/config/configuration-qemu.toml %{buildroot}/usr/share/defaults/kata-containers/configuration.toml

%clean

%files
/usr/bin/kata-runtime
/usr/bin/kata-proxy
/usr/bin/kata-shim
/usr/bin/kata-netmon
/var/lib/kata/kernel
/var/lib/kata/kata-containers-initrd.img
%config(noreplace) /usr/share/defaults/kata-containers/configuration.toml

%doc


%changelog
* Fri Nov 6 2020 yangfeiyu<yangfeiyu2@huawei.com> - 1.11.1-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revert the kata-containers.spec to still build kata-containers components into one package

* Fri Oct 9 2020 yangfeiyu<yangfeiyu2@huawei.com> - 1.11.1-5
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:directly copy kata binary files instead of building them

* Wed Sep 30 2020 yangfeiyu<yangfeiyu2@huawei.com> - 1.11.1-4
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:kata-runtime retry inserting of CNI interface

* Sun Sep 27 2020 LiangZhang<zhangliang5@Huawei.com> - 1.11.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix cmd params of direct use stratovirt binary

* Thu Sep 20 2020 jiangpengf<jiangpengfei9@huawei.com> - 1.11.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix del-iface doesn't delete the tap interface in the host problem

* Thu Aug 27 2020 jiangpengf<jiangpengfei9@huawei.com> - 1.11.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update kata-containers version to v1.11.1-1
