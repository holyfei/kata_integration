%global debug_package %{nil}
%define VERSION v1.0.0
%define RELEASE 2

Name:           kata-integration
Version:        %{VERSION}
Release:        %{RELEASE}
Summary:        Kata Container integration
License:        Apache 2.0
URL:            https://gitee.com/openeuler/kata_integration
Source0:        https://gitee.com/openeuler/kata_integration/repository/archive/v1.0.0?format=tar.gz#/%{name}-%{version}.tar.gz

BuildRoot:      %_topdir/BUILDROOT
BuildRequires: automake gcc glibc-devel glibc-static patch 

%description
This is a usefult tool for building Kata Container components.

%prep
%setup -q -c -a 0 -n %{name}-%{version}

%build

%clean

%files

%doc

%changelog
* Sat Sep 5 2020 jiangpengf<jiangpengfei9@huawei.com> - 1.0.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change Source format to URL

* Wed Aug 26 2020 jiangpengf<jiangpengfei9@huawei.com> - 1.0.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add initial kata-integration.spec
